# Caracteristics
- Only french text for now
- Only using llama3

# Requirement
- Download the LLM model from and put it in the data folder: https://huggingface.co/bartowski/Meta-Llama-3-8B-Instruct-GGUF/blob/main/Meta-Llama-3-8B-Instruct-Q6_K.gguf

# Install this for GPU usage
```
CUDACXX=/usr/local/cuda-12/bin/nvcc CMAKE_ARGS="-DLLAMA_CUBLAS=on -DCMAKE_CUDA_ARCHITECTURES=all-major" FORCE_CMAKE=1 pip install llama-cpp-python --no-cache-dir --force-reinstall --upgrade
```

# Launch the server with
```
lmql serve-model llama.cpp:data/Meta-Llama-3-8B-Instruct-IQ3_M.gguf --n_ctx 8000 --n_gpu_layers 32   
```

# Start a game
- First launch the preparation phase with the list of wikipedia article (prepare_questions.py)
- Then launch the game (launch_game.py)
