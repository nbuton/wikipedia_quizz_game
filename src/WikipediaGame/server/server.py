from WikipediaGame.game_object.game import WikipediaGame
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
from flask import redirect

app = Flask(__name__)
socketio = SocketIO(app)
game = WikipediaGame()
game.select_a_theme()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/Board")
def board():
    game_state = game.get_state()
    question = game.get_question()
    text_content = game.text_content
    players_scores = game.get_player_score()
    return render_template(
        "board.html",
        game_state=game_state,
        question=question,
        text_content=text_content,
        players_scores=players_scores,
    )


@app.route("/Player")
def player():
    game_state = game.get_state()
    ip_address = str(request.remote_addr)
    question = game.get_question()
    previous_answer = game.previous_player_answer(ip_address)
    end_question = game.all_player_answer()
    if end_question:
        emit("question_finish", namespace="/", broadcast=True)
        emit("reload_page", "Player", namespace="/", broadcast=True)
    return render_template(
        "player.html",
        game_state=game_state,
        question=question,
        end_question=end_question,
        previous_answer=previous_answer,
    )


@app.route("/submit_answer")
def submit_answer():
    ip_address = str(request.remote_addr)
    answer = request.args.get("answer")
    game.submit_answer(ip_address, answer)
    return redirect("/Player")


@app.route("/start_game")
def start_game():
    game.start()
    question = game.get_question()
    emit("new_question", question, namespace="/", broadcast=True)
    return redirect("/Board")


@app.route("/Add_player", methods=["GET"])
def add_player():
    ip_address = str(request.remote_addr)
    pseudo = request.args.get("pseudo")
    game.add_player(pseudo, ip_address)
    emit("player_join", {"pseudo": pseudo}, namespace="/", broadcast=True)
    return redirect("/Player")


@app.route("/reset_game", methods=["GET"])
def reset_game():
    game.new_theme()
    emit("reload_page", "All", namespace="/", broadcast=True)
    return redirect("/Board")


@socketio.on("get_next_question")
def handle_next_question():
    game.next_question()
    emit("reload_page", "All", namespace="/", broadcast=True)


app.run(host="192.168.1.4")
