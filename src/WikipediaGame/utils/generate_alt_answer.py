import lmql
import os

from WikipediaGame.utils.utils import read_openai_key


os.environ["OPENAI_API_KEY"] = read_openai_key()
# lmql.model("openai/text-babbage-002")


@lmql.query(model="openai/gpt-3.5-turbo-instruct")
def generate_false_alternative_answer(question, réponse):
    """lmql
    "Question : {question}\n"
    "Réponse : {réponse}\n"
    "\n"
    "Pour générer un QCM, voici 3 réponses totalement fausses à la question qui sont de taille similaire à la véritable réponse :"
    "\n"
    "1. [FAUSSE_RÉPONSE_1]\n" where STOPS_AT(FAUSSE_RÉPONSE_1, "\n")
    "2. [FAUSSE_RÉPONSE_2]\n" where STOPS_AT(FAUSSE_RÉPONSE_2, "\n")
    "3. [FAUSSE_RÉPONSE_3]\n" where STOPS_AT(FAUSSE_RÉPONSE_3, "\n")
    """
    return {
        "FAUSSE_RÉPONSE_1": FAUSSE_RÉPONSE_1,
        "FAUSSE_RÉPONSE_2": FAUSSE_RÉPONSE_2,
        "FAUSSE_RÉPONSE_3": FAUSSE_RÉPONSE_3,
    }
