def read_openai_key():
    with open("data/OPENAI_KEY") as fichier:
        API_KEY = fichier.readlines()[0]
    return API_KEY
