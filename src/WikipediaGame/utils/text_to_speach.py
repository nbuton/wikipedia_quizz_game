from TTS.api import TTS
from textwrap import wrap


def text_to_speech_v2(text, output_path, device="cpu"):
    text_list = wrap(text, 250)
    tts = TTS("tts_models/multilingual/multi-dataset/xtts_v2").to(device)
    tts.tts_to_file(
        text=text_list,
        speaker_wav="data/example_voice_fr.wav",
        language="fr",
        file_path=output_path,
    )
