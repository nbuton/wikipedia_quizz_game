import random
from llama_index.core.llama_dataset.generator import RagDatasetGenerator
from llama_index.llms.openai import OpenAI

from llama_index.llms.openai import OpenAI
from llama_index.core import Document
import pandas as pd


from WikipediaGame.prompt.prompt import (
    DEFAULT_QUESTION_GENERATION_PROMPT,
    DEFAULT_TEXT_QA_PROMPT,
    get_prompt_question_gen_query,
)
from WikipediaGame.utils.utils import read_openai_key
from WikipediaGame.utils.generate_alt_answer import (
    generate_false_alternative_answer,
)

api_key = read_openai_key()


def generate_df_questions(content, num_questions_per_chunk):
    documents = [Document(text=content)]

    openai_llm = OpenAI(model="gpt-3.5-turbo", temperature=0.1, api_key=api_key)

    question_gen_query = get_prompt_question_gen_query(num_questions_per_chunk)

    dataset_generator = RagDatasetGenerator.from_documents(
        documents=documents,
        llm=openai_llm,
        num_questions_per_chunk=num_questions_per_chunk,
        text_question_template=DEFAULT_QUESTION_GENERATION_PROMPT,
        text_qa_template=DEFAULT_TEXT_QA_PROMPT,
        question_gen_query=question_gen_query,
        workers=1,
        show_progress=True,
    )

    rag_dataset = dataset_generator.generate_dataset_from_nodes()

    df_question_answer = rag_dataset.to_pandas()
    print("df_question_answer:", df_question_answer)
    dico_complete_df = {
        "question": [],
        "a": [],
        "b": [],
        "c": [],
        "d": [],
        "correct_answer": [],
    }
    list_possibilities = ["a", "b", "c", "d"]
    for _, row in df_question_answer.iterrows():
        question = row["query"]
        question = question.replace("\n", "")
        print("question:", question)
        correct_answer = row["reference_answer"]
        correct_answer = correct_answer.replace("\n", "")
        print("correct_answer:", correct_answer)
        dico_alt_wrong_answer = generate_false_alternative_answer(
            question,
            correct_answer,
        ).variables
        dico_alt_wrong_answer = {
            key: value.replace("\n", "").strip()
            for key, value in dico_alt_wrong_answer.items()
        }
        print("dico_alt_wrong_answer:", dico_alt_wrong_answer)
        dico_complete_df["question"].append(question)
        count = 0
        correct_ind = random.randint(0, 3)
        correct_letter = list_possibilities[correct_ind]
        dico_complete_df["correct_answer"].append(correct_letter)
        for letter in list_possibilities:
            if letter == correct_letter:
                dico_complete_df[letter].append(correct_answer)
            else:
                dico_complete_df[letter].append(
                    list(dico_alt_wrong_answer.values())[count]
                )
                count += 1

    df_complete_df = pd.DataFrame.from_dict(dico_complete_df)

    return df_complete_df
