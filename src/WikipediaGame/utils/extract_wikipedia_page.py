import requests
from bs4 import BeautifulSoup


def parse_wikipedia(url, title_tag="h2"):
    """
    Extracts sections from a webpage based on specified title tags and returns them as a dictionary.

    Args:
        url: The URL of the webpage to process.
        title_tag: The HTML tag to use for section titles (e.g., "h2", "h3"). Defaults to "h2".

    Returns:
        A dictionary where keys are title text content and values are ALL content within each section (including paragraphs and list items).
    """
    print("I will parse the following url:", url)
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise exception for non-200 status codes
    except requests.exceptions.RequestException as e:
        print(f"Error fetching url: {url} - {e}")
        return {}

    soup = BeautifulSoup(response.content, "html.parser")
    sections = {}

    # Loop through specified title elements
    for title in soup.find_all(title_tag):
        title_text = title.text.strip().replace("[modifier | modifier le code]", "")
        section_content = []

        # Find all elements within the next sibling until the next title
        next_sibling = title.next_sibling
        while next_sibling and next_sibling.name != title_tag:
            if next_sibling.name in ("p", "ul", "h1", "h2", "h3", "h4", "h5", "h6"):
                # Append paragraph text or traverse list items
                if next_sibling.name != "ul":
                    section_content.append(
                        next_sibling.text.strip().replace(
                            "[modifier | modifier le code]", ""
                        )
                    )
                else:
                    # Extract text from list items recursively
                    section_content.append(extract_list_content(next_sibling))
            next_sibling = next_sibling.next_sibling

        # Add section content to dictionary if content found
        if section_content:
            sections[title_text] = "\n".join(section_content)

    return sections


# Helper function to extract text content from list items
def extract_list_content(list_element):
    content = []
    for item in list_element.find_all("li"):
        content.append(item.text.strip())
    return "\n".join(content)
