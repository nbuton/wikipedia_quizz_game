from flask import Flask
import pandas as pd

from WikipediaGame.game_object.utils import check_answer, get_content, get_one_subject


class WikipediaGame:
    def __init__(self):
        self.state = "not_start"  # "question", "finish"
        self.map_ip_to_pseudo = {}
        self.score_each_ip = {}
        self.answer_each_player = {}
        self.ind_question = 0

    def get_player_score(self):
        return {
            self.map_ip_to_pseudo[ip]: score for ip, score in self.score_each_ip.items()
        }

    def all_player_answer(self):
        """
        All player have answer the question
        """
        return len(self.answer_each_player) == len(self.map_ip_to_pseudo)

    def previous_player_answer(self, ip):
        if ip in self.answer_each_player.keys():
            return self.answer_each_player[ip]
        else:
            return None

    def get_state(self):
        return self.state

    def add_player(self, pseudo, ip):
        self.map_ip_to_pseudo[ip] = pseudo
        self.score_each_ip[ip] = 0

    def start(self):
        self.state = "question"

    def submit_answer(self, ip, answer):
        if self.previous_player_answer(ip) is None:
            self.answer_each_player[ip] = answer
            is_correct = check_answer(self.df_MCQ, self.ind_question, answer)
            if is_correct:
                self.score_each_ip[ip] += 1

    def get_question(self):
        current_question = self.df_MCQ.iloc[self.ind_question].to_dict()
        current_question["question_num"] = current_question["Unnamed: 0"]
        current_question["nb_total_question"] = len(self.df_MCQ)
        print("current_question:", current_question)
        return current_question

    def select_a_theme(self):
        subject_folder = "data/subjects/"
        subject = get_one_subject(subject_folder)
        path_this_subject = subject_folder + subject + "/"
        self.text_content = get_content(path_this_subject)
        self.df_MCQ = pd.read_csv(path_this_subject + "df_MCQ.csv")
        # self.df_MCQ = self.df_MCQ.sample(n=3)

    def next_question(self):
        self.answer_each_player = {}
        if self.ind_question < len(self.df_MCQ) - 1:
            self.ind_question += 1
        else:
            self.state = "finish"

    def new_theme(self):
        self.state = "not_start"
        self.score_each_ip = {ip: 0 for ip in self.score_each_ip.keys()}
        self.answer_each_player = {}
        self.ind_question = 0
        self.select_a_theme()
