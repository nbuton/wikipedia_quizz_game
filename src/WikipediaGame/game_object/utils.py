import os


def get_one_subject(subjet_folder):
    list_folder = os.listdir(subjet_folder)
    for index, folder_name in enumerate(list_folder):
        print(index, ":", folder_name)
    ind_folder = input("Give the index of the folder you want: ")
    ind_folder = int(ind_folder)
    return list_folder[ind_folder]


def get_content(path_this_subject):
    with open(path_this_subject + "/content_paragraph.txt") as fichier:
        content = fichier.read()
    return content


def check_answer(df_MCQ, question_index, selected_answer_letter):
    question = df_MCQ.iloc[int(question_index)]
    correct_answer_letter = question["correct_answer"]
    is_correct = selected_answer_letter == correct_answer_letter
    return is_correct
