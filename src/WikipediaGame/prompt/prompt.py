from llama_index.core.prompts.base import PromptTemplate
from llama_index.core.prompts.prompt_type import PromptType

DEFAULT_QUESTION_GENERATION_PROMPT = PromptTemplate(
    """\
Les informations contextuelles sont ci-dessous.
---------------------
{context_str}
---------------------
Étant donné les informations contextuelles et aucune connaissance préalable,
générez uniquement des questions basées sur la requête suivante.
{query_str}
"""
)
DEFAULT_TEXT_QA_PROMPT_TMPL = (
    "Les informations contextuelles sont ci-dessous.\n"
    "---------------------\n"
    "{context_str}\n"
    "---------------------\n"
    "Étant donné les informations contextuelles et aucune connaissance préalable, "
    "répondez à la question.\n"
    "Question: {query_str}\n"
    "Réponse: "
)

DEFAULT_TEXT_QA_PROMPT = PromptTemplate(
    DEFAULT_TEXT_QA_PROMPT_TMPL, prompt_type=PromptType.QUESTION_ANSWER
)


def get_prompt_question_gen_query(num_questions_per_chunk):
    question_gen_query = f"Vous êtes un(e) professeur. Votre tâche consiste à préparer {num_questions_per_chunk} questions pour un prochain quizz. Limitez les questions aux informations contextuelles fournies. Les questions doivent être simple et ne pas être trop longue."
    return question_gen_query
