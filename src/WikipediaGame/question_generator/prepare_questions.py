import os

import numpy as np
from urllib.parse import unquote

from WikipediaGame.utils.extract_wikipedia_page import parse_wikipedia
from WikipediaGame.utils.generate_questions import generate_df_questions
from WikipediaGame.utils.text_to_speach import text_to_speech_v2


def hamming_distance(chaine1, chaine2):
    return sum(c1 != c2 for c1, c2 in zip(chaine1, chaine2))


def get_most_similar_section(all_keys, section):
    best_key = None
    min_dist = np.inf
    for key in all_keys:
        distance = hamming_distance(key, section)
        if distance < min_dist:
            min_dist = distance
            best_key = key
    return best_key


def get_begining_of_content_of_too_long(content, max_nb_lines):
    # content = content.encode("utf-8").strip()
    # content = content.replace("\xa0", " ")
    tab = content.split(".")
    return ".".join(tab[:max_nb_lines])


dico_wikipedia_link_and_section = {
    # "https://fr.wikipedia.org/wiki/Cin%C3%A9ma": "Histoire",
    "https://fr.wikipedia.org/wiki/Made_in_Abyss": "Histoire",
    "https://fr.wikipedia.org/wiki/Jeu_vid%C3%A9o": "Histoire",
    "https://fr.wikipedia.org/wiki/Lindsey_Stirling": "Biographie",
    "https://fr.wikipedia.org/wiki/Italie": "Géographie",
    "https://fr.wikipedia.org/wiki/%C3%89cologie": "Définition",
    "https://fr.wikipedia.org/wiki/Philosophie": "La philosophie occidentale",
    "https://fr.wikipedia.org/wiki/Musique": "Histoire",
}

for wikipedia_url, section in dico_wikipedia_link_and_section.items():
    subject = wikipedia_url.split("/")[-1]
    subject = unquote(subject)
    subect_folder = "data/subjects/" + subject
    os.mkdir(subect_folder)
    parse_article = parse_wikipedia(wikipedia_url)
    choosen_section = get_most_similar_section(parse_article.keys(), section)
    section_content = parse_article[choosen_section]

    section_content = get_begining_of_content_of_too_long(
        section_content, max_nb_lines=20  # 10
    )
    print("section_content:", section_content)
    fichier = open(subect_folder + "/content_paragraph.txt", "w")
    fichier.write(section_content)
    fichier.close()

    df_MCQ = generate_df_questions(section_content, num_questions_per_chunk=5)
    df_MCQ.to_csv(subect_folder + "/df_MCQ.csv")
    # text_to_speech_v2(
    #     text=section_content,
    #     output_path=subect_folder + "/audio.wav",
    #     device="cuda",
    # )
