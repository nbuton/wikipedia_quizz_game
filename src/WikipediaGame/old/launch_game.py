import os
import pandas as pd
from flask import Flask, render_template, request
import flask

app = Flask(__name__)
wikiepdia_url = None
df_MCQ = None
choosen_section = None
text_content = None
path_this_subject = None
users_points = {}
current_question_index_dict = {}
nb_players = 2


@app.route("/final_results", methods=["GET"])
def end_game_page():
    return render_template(
        "final_results.html",
        result={
            "dico_score_all_participant": users_points,
        },
    )


@app.route("/question_suivante", methods=["GET"])
def question_suivante():
    ip_address = str(request.remote_addr)
    current_question_index_dict[ip_address] += 1
    return question_page()


@app.route("/quizz", methods=["GET"])
def question_page():
    global users_points
    global current_question_index_dict
    global df_MCQ
    ip_address = str(request.remote_addr)
    if ip_address not in current_question_index_dict.keys():
        current_question_index_dict[ip_address] = 0

    selected_answer_letter = request.args.get("answer")

    current_question_index = current_question_index_dict[ip_address]
    end_game = current_question_index_dict[ip_address] >= len(df_MCQ)
    if end_game:
        return render_template(
            "final_results.html",
            result={
                "dico_score_all_participant": users_points,
            },
        )

    current_question = df_MCQ.iloc[current_question_index].to_dict()
    current_question["current_question_index"] = current_question_index
    current_question["total_nb_question"] = len(df_MCQ)
    current_question["wikipedia_url"] = wikiepdia_url
    current_question["choosen_section"] = choosen_section
    if selected_answer_letter is None:
        current_question["correct_answer"] = None
        current_question["selected_answer"] = None
    else:
        question_index = request.args.get("current_question_index")
        correct_answer_letter, is_correct = check_answer(
            question_index, selected_answer_letter
        )
        print("correct_answer_letter:", correct_answer_letter)
        if ip_address in users_points.keys():
            users_points[ip_address] += int(is_correct)
        else:
            users_points[ip_address] = int(is_correct)
        # current_question_index_dict[ip_address] += 1
        current_question["correct_answer_letter"] = correct_answer_letter
        current_question["selected_answer_letter"] = selected_answer_letter

    return render_template("mcq.html", question=current_question)


@app.route("/", methods=["GET"])
def start_page():
    test = flask.url_for("static", filename=path_this_subject + "/audio.wav")
    print("test:", test)
    data = {
        "text_content": text_content,
        "path_subject_folder": path_this_subject,
    }
    return render_template("index.html", data=data)


if __name__ == "__main__":
    subject_folder = "data/subjects/"
    subject = get_one_subject(subject_folder)
    path_this_subject = subject_folder + subject + "/"
    text_content = get_content(path_this_subject)
    df_MCQ = pd.read_csv(path_this_subject + "df_MCQ.csv")

    app.run(host="192.168.1.4")
